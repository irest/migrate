
.PHONY: install test

packages := $(shell go list ./... | grep -v /vendor/)

install:
	@go get -u -v github.com/golang/dep/cmd/dep
	@dep ensure

test:
	@go test ./...
	@for package in $(packages); do \
            mkdir -p cover; \
            go test -covermode=count -coverprofile "cover/$${package##*/}.cov" "$${package}"; \
            done
	@echo "mode: count" > cover/coverage.cov
	@tail -q -n +2 cover/*.cov >> cover/coverage.cov
	go tool cover -func=cover/coverage.cov
	@go tool cover -html=cover/coverage.cov -o cover/coverage.html