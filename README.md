# migrate
[![pipeline status](https://gitlab.com/irest/migrate/badges/master/pipeline.svg)](https://gitlab.com/irest/migrate/commits/master)
[![coverage report](https://gitlab.com/irest/migrate/badges/master/coverage.svg)](https://gitlab.com/irest/migrate/commits/master)

A simple embedable migration tool.

**Requirements:**
- go 1.10+
- mysql 5.7+

**Documentation:**
- [godoc](https://godoc.org/gitlab.com/irest/migrate)

**Installation:**
```bash
go get -u gitlab.com/irest/migrate/...
```

**HOWTO:**

- Create `migrations` folder in your project root: 
  ```bash
  mkdir migrations
  ```
- cd into `migrations` folder and run:
  ```bash
  irest-migrate -create -name my_first_migration
  ```
- You should now see a file called something like: `1539777753_my_first_migration.go`. Go and edit it with your favorite editor. Fill the `Up` and `Down` functions with stuff you need to do. 
  
  *Example:*
  ```go
  package migrations
  
  import (
  	"database/sql"
  
  	"gitlab.com/irest/migrate"
  )
  
  func init() {
  	migrate.Register(1539777753, &migrate.Migration{
  		Name: "my_first_migration",
  		Up: func(t *sql.Tx) error {
  			// Up code goes here
  
  		},
  		Down: func(t *sql.Tx) error {
  			// Down code goes here
  
  		},
  	})
  }
  ```
- Find a place in your app where you want the migrations to run and add the following:
  ```go
  if err := migrate.Up(db); err != nil {
      log.Fatal("migrations failed: ", err)
  }
  ```
  *Example:*
  ```go
  package main
  
  import (
    "database/sql"
    "gitlab.com/irest/migrate"
    _ "github.com/go-sql-driver/mysql"
  )

  func main() {
    db, err := sql.Open("mysql", "root@tcp(localhost:3306)/db?parseTime=true")
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()
    if err := migrate.Up(db); err != nil {
        log.Fatal("migrations failed: ", err)
    }
    // the database is now updated
  }
  ```