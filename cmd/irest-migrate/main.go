package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"time"
)

const (
	filePerm    = 0644
	namePattern = "([a-zA-Z0-9_-]+)"
)

var (
	cwd        string
	err        error
	nameRegExp = regexp.MustCompile("^" + namePattern + "$")
	fileRexExp = regexp.MustCompile("^([0-9]+)_" + namePattern + `\.go$`)
	create     = flag.Bool("create", false, "Create a new migration")
	name       = flag.String("name", "", "Migration name")
)

func main() {
	flag.Parse()
	cwd, err = os.Getwd()
	if err != nil {
		printErr("Could not detect CWD")
	}
	switch true {
	case *create:
		doCreate()
		return
	}
	flag.Usage()
}

func doCreate() {
	// migration name cannot be empty
	if *name == "" {
		printErr("A new migration should have a name")
	}
	// migration name should match name regexp
	if !nameRegExp.MatchString(*name) {
		printErr(fmt.Sprintf(
			"Invalid name \"%s\", the name should match %s",
			*name,
			nameRegExp))
	}
	// generate contents and save to file
	ts := time.Now().Unix()
	fileName := newFileName(*name, ts)
	contents := newFileContents(*name, ts)
	writeFile(fileName, contents)
	fmt.Printf("Created migration file: %s\n", fileName)
}

func newFileName(name string, ts int64) string {
	return fmt.Sprintf("%d_%s.go", time.Now().Unix(), name)
}

func newFileContents(name string, ts int64) []byte {
	packageName := path.Base(cwd)
	return []byte(fmt.Sprintf(tpl, packageName, ts, name))
}

func writeFile(fileName string, contents []byte) {
	fullPath := path.Join(cwd, fileName)
	if err := ioutil.WriteFile(fullPath, contents, filePerm); err != nil {
		printErr("Failed to write migration file: " + err.Error())
	}
}

func printOK(msg string) {
	fmt.Println(msg)
}

func printErr(err string) {
	printOK(err)
	os.Exit(1)
}

var tpl = `package %s

import (
	"database/sql"

	"gitlab.com/irest/migrate"
)

func init() {
	migrate.Register(%d, &migrate.Migration{
		Name: "%s",
		Up: func(t *sql.Tx) error {
			// Up code goes here
			return nil
		},
		Down: func(t *sql.Tx) error {
			// Down code goes here
			return nil
		},
	})
}
`
