package migrate

import (
	"database/sql"
	"errors"
	"sort"
	"sync"
	"time"
)

const (
	sqlDatetimeFormat = "2006-01-02 15:04:05"
)

var (
	lastStack  uint64
	migrations = map[uint64]*Migration{}
	executed   = []uint64{}
	mutex      = new(sync.Mutex)
)

// Up finds new migrations and executes them
func Up(db *sql.DB) error {
	if err := initialize(db); err != nil {
		return err
	}
	ts := time.Now()
	tx, err := db.Begin()
	if err != nil {
		return err
	}
	stack := lastStack + 1
	list := getNew()
	for id, m := range list {
		if m.Up == nil {
			continue
		}
		if err := m.Up(tx); err != nil {
			return err
		}
		m.executed = true
		migrations[id].executed = true
		_, err := tx.Exec(`
			INSERT INTO _migrate (
				id, name, stack, executed_at
			) VALUES (
				?, ?, ?, ?
			);
		`, id, m.Name, stack, ts)
		if err != nil {
			return err
		}
	}
	if err := tx.Commit(); err != nil {
		return err
	}
	lastStack = stack
	return nil
}

func Down(db *sql.DB) error {
	return nil
}

// Register a new migration
func Register(id uint64, migration *Migration) {
	migration.executed = false
	migrations[id] = migration
}

func initialize(db *sql.DB) error {
	// create migrations table
	if err := createTable(db); err != nil {
		return err
	}
	// load executed migrations
	if err := loadExecuted(db); err != nil {
		return err
	}
	return nil
}

func createTable(db *sql.DB) error {
	_, err := db.Exec(`
		CREATE TABLE IF NOT EXISTS _migrate (
			id BIGINT(18) NOT NULL,
			name VARCHAR(255) NOT NULL,
			stack BIGINT(18) NOT NULL,
			executed_at DATETIME NOT NULL,
			PRIMARY KEY(id)
		) ENGINE=InnoDB;
	`)
	return err
}

func loadExecuted(db *sql.DB) error {
	rows, err := db.Query(`
		SELECT id, stack FROM _migrate;
	`)
	if err != nil {
		return err
	}
	for rows.Next() {
		var (
			id    uint64
			stack uint64
		)
		if err := rows.Scan(&id, &stack); err != nil {
			return err
		}
		if id < 1 {
			return errors.New("failed to scan migrations table")
		}
		if stack > lastStack {
			lastStack = stack
		}
		executed = append(executed, id)
	}
	if err := rows.Err(); err != nil {
		return err
	}
	updateMigrations()
	return nil
}

func updateMigrations() {
	// remove executed from migrations
	for _, id := range executed {
		if m, ok := migrations[id]; ok {
			m.executed = true
		}
	}
}

func getNew() map[uint64]*Migration {
	list := map[uint64]*Migration{}
	ids := make([]uint64, 0)
	for id := range migrations {
		ids = append(ids, id)
	}
	sort.Slice(ids, func(a, b int) bool {
		return ids[a] < ids[b]
	})
	for _, id := range ids {
		if !migrations[id].executed {
			list[id] = migrations[id]
		}
	}
	return list
}

// Migration object
type Migration struct {
	Name      string
	Up        Func
	Down      Func
	executed  bool
	createdAt time.Time
}

// Func represents a migration func
type Func func(*sql.Tx) error
