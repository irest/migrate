package migrate

import (
	"database/sql"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

func TestStart(t *testing.T) {
	suite.Run(t, new(migrateTestSuite))
}

type migrateTestSuite struct {
	suite.Suite
	db   *sql.DB
	mock sqlmock.Sqlmock
}

func (t *migrateTestSuite) SetupTest() {
	migrations = map[uint64]*Migration{}
	executed = []uint64{}
	t.db, t.mock = t.prepareDB()
}

func (t *migrateTestSuite) TestInit() {
	t.Lenf(migrations, 0, "migration should be empty on init")
	t.Lenf(executed, 0, "executed should be empty on init")
}

func (t *migrateTestSuite) TestRegister() {
	m1 := &Migration{}
	Register(123, m1)
	t.Equal(1, len(migrations), "migrations len should increase")
	t.Equal(m1, migrations[123], "m1 migration should be added to migrations")
	m2 := &Migration{}
	Register(456, m2)
	t.Equal(2, len(migrations), "migrations len should increase")
	t.Equal(m2, migrations[456], "m2 migration should be added to migrations")
}

func (t *migrateTestSuite) TestCrateTableSuccess() {
	t.mock.
		ExpectExec("CREATE TABLE IF NOT EXISTS _migrate").
		WillReturnResult(sqlmock.NewResult(1, 1))
	t.NoError(createTable(t.db))
	t.checkSQL()
}

func (t *migrateTestSuite) TestCrateTableFail() {
	e := errors.New("sql error")
	t.mock.
		ExpectExec("CREATE TABLE IF NOT EXISTS _migrate").
		WillReturnError(e)
	t.Error(createTable(t.db))
	t.checkSQL()
}

func (t *migrateTestSuite) TestLoadExecutedSuccess() {
	rows := sqlmock.
		NewRows([]string{"id", "stack"}).
		AddRow(1, 12).
		AddRow(2, 12).
		AddRow(3, 12)
	t.mock.
		ExpectQuery("SELECT id, stack FROM _migrate").
		WillReturnRows(rows)
	err := loadExecuted(t.db)
	t.NoError(err)
	t.Nil(err)
	t.checkSQL()
}

func (t *migrateTestSuite) TestLoadExecutedFail() {
	t.mock.
		ExpectQuery("SELECT id, stack FROM _migrate").
		WillReturnError(errors.New("sql error"))
	err := loadExecuted(t.db)
	t.Error(err)
	t.EqualError(err, "sql error")
}

func (t *migrateTestSuite) TestLoadExecutedScan() {
	rows := sqlmock.
		NewRows([]string{"id", "stack"}).
		AddRow("abc", 12)
	t.mock.
		ExpectQuery("SELECT id, stack FROM _migrate").
		WillReturnRows(rows)
	t.Error(loadExecuted(t.db))
	t.checkSQL()
}

func (t *migrateTestSuite) TestLoadExecutedIDValid() {
	rows := sqlmock.
		NewRows([]string{"id", "stack"}).
		AddRow(0, 12)
	t.mock.
		ExpectQuery("SELECT id, stack FROM _migrate").
		WillReturnRows(rows)
	err := loadExecuted(t.db)
	t.Error(err)
	t.EqualError(err, "failed to scan migrations table")
	t.checkSQL()
}

func (t *migrateTestSuite) TestLoadExecutedRowsErr() {
	rows := sqlmock.
		NewRows([]string{"id", "stack"}).
		AddRow(1, 12).
		RowError(0, errors.New("row error"))
	t.mock.
		ExpectQuery("SELECT id, stack FROM _migrate").
		WillReturnRows(rows)
	err := loadExecuted(t.db)
	t.Error(err)
	t.EqualError(err, "row error")
	t.checkSQL()
}

func (t *migrateTestSuite) TestUpdateMigrations() {
	Register(4, new(Migration))
	Register(6, new(Migration))
	Register(5, new(Migration))
	Register(1, new(Migration))
	Register(2, new(Migration))
	Register(3, new(Migration))
	for _, m := range migrations {
		t.False(m.executed)
	}
	rows := sqlmock.
		NewRows([]string{"id", "stack"}).
		AddRow(1, 1).
		AddRow(2, 1).
		AddRow(3, 1)
	t.mock.ExpectExec("CREATE TABLE").WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectQuery("SELECT id, stack").WillReturnRows(rows)
	t.NoError(initialize(t.db))
	t.Lenf(migrations, 6, "expecting migrations len to be 6")
	t.Lenf(executed, 3, "expecting executed len to be 3")
	t.True(migrations[1].executed)
	t.True(migrations[2].executed)
	t.True(migrations[3].executed)
	t.checkSQL()
}

func (t *migrateTestSuite) TestInitializeCreateError() {
	t.mock.ExpectExec("CREATE TABLE").WillReturnError(errors.New("create error"))
	err := initialize(t.db)
	t.Error(err)
	t.EqualError(err, "create error")
}

func (t *migrateTestSuite) TestInitializeSelectError() {
	t.mock.ExpectExec("CREATE TABLE").WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectQuery("SELECT id, stack").WillReturnError(errors.New("select error"))
	err := initialize(t.db)
	t.Error(err)
	t.EqualError(err, "select error")
}

func (t *migrateTestSuite) TestGetNew() {
	m4 := new(Migration)
	m6 := new(Migration)
	m5 := new(Migration)
	m1 := new(Migration)
	m2 := new(Migration)
	m3 := new(Migration)
	Register(4, m4)
	Register(6, m6)
	Register(5, m5)
	Register(1, m1)
	Register(2, m2)
	Register(3, m3)
	for _, m := range migrations {
		t.False(m.executed)
	}
	rows := sqlmock.
		NewRows([]string{"id", "stack"}).
		AddRow(1, 1).
		AddRow(2, 1).
		AddRow(3, 1)
	t.mock.ExpectExec("CREATE TABLE").WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectQuery("SELECT id, stack").WillReturnRows(rows)
	t.NoError(initialize(t.db))
	t.Lenf(migrations, 6, "expecting migrations len to be 6")
	t.Lenf(executed, 3, "expecting executed len to be 3")
	list := getNew()
	t.IsType(map[uint64]*Migration{}, list)
	t.Equal(list[4], m4)
	t.Equal(list[5], m5)
	t.Equal(list[6], m6)
}

func (t *migrateTestSuite) testUpPrepare() {
	Register(1, new(Migration))
	Register(6, new(Migration))
	Register(3, new(Migration))
	Register(2, new(Migration))
	Register(4, &Migration{
		Up: func(tx *sql.Tx) error {
			_, err := tx.Exec("UPDATE b")
			t.NoError(err)
			return err
		},
	})
	Register(5, &Migration{
		Up: func(tx *sql.Tx) error {
			_, err := tx.Exec("UPDATE a")
			t.NoError(err)
			return err
		},
	})
	for _, m := range migrations {
		t.False(m.executed)
	}
	rows := sqlmock.
		NewRows([]string{"id", "stack"}).
		AddRow(1, 1).
		AddRow(2, 1).
		AddRow(3, 1)
	t.mock.ExpectExec("CREATE TABLE").WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectQuery("SELECT id, stack").WillReturnRows(rows)
}

func (t *migrateTestSuite) TestUp() {
	t.testUpPrepare()
	t.mock.ExpectBegin()
	t.mock.ExpectExec("UPDATE b").WillReturnResult(sqlmock.NewResult(10, 20))
	t.mock.ExpectExec("INSERT").WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectExec("UPDATE a").WillReturnResult(sqlmock.NewResult(10, 20))
	t.mock.ExpectExec("INSERT").WillReturnResult(sqlmock.NewResult(1, 1))
	t.mock.ExpectCommit()
	t.NoError(Up(t.db))
	t.checkSQL()
}

func (t *migrateTestSuite) prepareDB() (*sql.DB, sqlmock.Sqlmock) {
	dsn := fmt.Sprintf("sqlmock_%d?parseTime=true", time.Now().UnixNano())
	db, mock, err := sqlmock.NewWithDSN(dsn)
	t.NoError(err, "an error was not expected when opening a stub database connection")
	return db, mock
}

func (t *migrateTestSuite) checkSQL() {
	t.NoError(t.mock.ExpectationsWereMet(), "there were unfulfilled expectations")
}
